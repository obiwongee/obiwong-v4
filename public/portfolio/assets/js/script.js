function getCurrentTrack()
{
    $.ajax({
        url: '/api/current-song',
        success: function (res) {
            if (!jQuery.isEmptyObject(res)) {
                let artist = res.artist;
                let title = res.title;
                let url = res.url;

                let currentSong = $('<a>')
                    .attr('href', url)
                    .attr('target', '_blank')
                    .html(artist + ' - ' + title);

                $('#current-song').html('');
                $('#current-song').append(currentSong);
                $('#current-song-container').css('visibility', 'visible');
            }
        }
    });
}

$(document).ready(function() {
    /**
     * Make links open in new window
     */
    $('.row').find('a').each(function() {
        if (!this.hasAttribute('target')) {
            $(this).attr('target', '_blank');
        }
    });

    /**
     * Current Track
     */
    getCurrentTrack();
    setInterval(function() {
        getCurrentTrack();
    }, 90000);

    /**
     * Spotify album art
     */
    $('.spotify-album').each(function() {
        $(this).attr('src', $(this).data('src'));
    });

    /**
     * Enable Bootstrap tooltips
     */
    $('[data-toggle="tooltip"]').tooltip();

    /**
     * YouTube favourite Song
     */
    // Gets the video src from the data-src on each button
    $('.video-btn').click(function() {
        $('.back-to-top').css('display', 'none');
        $('.video').fadeIn();
        $('.video iframe').attr('src', $(this).data('src') + '?modestbranding=1&amp;showinfo=0&amp;autoplay=1');

        return false;
    });

    $('.video-close').click(function() {
        $('.back-to-top').css('display', 'inline');
        $('.video').hide();
        $('.video iframe').attr('src', '');

        return false;
    });

    /**
     * Hero animation
     */
    setTimeout(function() {
        $('.hero-container').addClass('animate__tada');
    }, 1000);

    /**
     * Coin flip profile picture
     */
    var counter = 0;
    var animationEnd = (function(el) {
        var animations = {
            animation: 'animationend',
            OAnimation: 'oAnimationEnd',
            MozAnimation: 'mozAnimationEnd',
            WebkitAnimation: 'webkitAnimationEnd',
        };

        for (var t in animations) {
            if (el.style[t] !== undefined) {
                return animations[t];
            }
        }
    })(document.createElement('div'));

    $('#profile').on(animationEnd, function() {
        $('#profile').removeClass('animate__flip');
    });

    setInterval(function() {
        var index = counter % pictures.length;
        $('#profile').attr('src', '/portfolio/assets/img/' + pictures[index]);
        counter++;
        $('#profile').addClass('animate__flip');
    }, 5000);
});

$(window).on('load', function() {
    let fTime = (Date.now() - start);
    let bTime = parseInt($('#backend-load-time').html());
    let total = (fTime + bTime) / 1000;

    if (bTime > 1000) {
        bTime = bTime / 1000;
        $('#backend-load-time').html(bTime.toFixed(2) + 's');
    }

    if (fTime > 1000) {
        fTime = fTime / 1000;
        $('#frontend-load-time').html(fTime.toFixed(2) + 's');
    } else {
        $('#frontend-load-time').html(fTime + 'ms');
    }

    $('#total-load-time').html(total.toFixed(2) + 's');
});