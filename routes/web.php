<?php

use App\Http\Controllers\HypernautsController;
use App\Http\Controllers\SpotifyController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\DownloadController;
use App\Http\Controllers\FightcadeController;
use \App\Http\Controllers\LinksController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PortfolioController::class, 'index']);
Route::get('/download/{filename}', [DownloadController::class, 'index']);
Route::get('/fightcade', [FightcadeController::class, 'index']);
Route::get('/links', [LinksController::class, 'index']);

Route::group(['middleware' => ['dev']], function() {
    Route::get('/spotify', [SpotifyController::class, 'index']);
    Route::get('/spotify/callback', [SpotifyController::class, 'callback']);
});