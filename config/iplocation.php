<?php

return [
    'ipgeolocation' => [
        'url' => env('IPGEOLOCATION_URL', 'https://api.ipgeolocation.io'),
        'api_key' => env('IPGEOLOCATION_API_KEY'),
    ],
    'openweather' => [
        'url' => env('OPENWEATHER_URL', 'https://api.openweathermap.org/data/2.5'),
        'api_key' => env('OPENWEATHER_API_KEY'),
    ],
    'test_ip' => env('TEST_IP'),
];
