<?php

return [
    'api_base_url' => env('MOXFIELD_API_BASE_URL'),
    'asset_base_url' => env('MOXFIELD_ASSET_BASE_URL'),
    'asset_prefix' => env('MOXFIELD_ASSET_PREFIX'),
    'asset_suffix' => env('MOXFIELD_ASSET_SUFFIX'),
];