<?php


return [
    'client_id' => env('SPOTIFY_CLIENT_ID'),
    'encrypted_secret' => base64_encode(env('SPOTIFY_CLIENT_ID'). ':' . env('SPOTIFY_CLIENT_SECRET')),
    'redirect_uri' => env('SPOTIFY_REDIRECT_URI'),
    'scopes' => env('SPOTIFY_SCOPES'),
    'refresh_token' => env('SPOTIFY_REFRESH_TOKEN'),
];