<?php

return [
    'base_url' => env('RIOT_BASE_URL', 'https://americas.api.riotgames.com/tft/match/v1'),
    'puuid' => env('RIOT_PUUID'),
    'api_key' => env('RIOT_API_KEY'),
    'fetch_count' => env('RIOT_MATCH_FETCH_COUNT', 20),
    'summoner_id' => env('RIOT_SUMMONER_ID')
];
