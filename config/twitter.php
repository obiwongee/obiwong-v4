<?php

return [
    'screen_name' => env('TWITTER_SCREEN_NAME'),
    'fetch_count' => env('TWITTER_FETCH_COUNT'),
];
