<!DOCTYPE html>
<html lang="en" >

<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

    <title>obiwong.com - Stuff I Made</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <style>
        body {
            width: 100wh;
            height: 90vh;
            color: #fff;
            background-image: linear-gradient(to left bottom, #cc52de, #9643ac, #66327a, #3b204a, #180c1e);
            background-size: 400% 400%;
            -webkit-animation: Gradient 15s ease infinite;
            -moz-animation: Gradient 15s ease infinite;
            animation: Gradient 15s ease infinite;
        }

        @-webkit-keyframes Gradient {
            0% {
                background-position: 0% 50%
            }
            50% {
                background-position: 100% 50%
            }
            100% {
                background-position: 0% 50%
            }
        }

        @-moz-keyframes Gradient {
            0% {
                background-position: 0% 50%
            }
            50% {
                background-position: 100% 50%
            }
            100% {
                background-position: 0% 50%
            }
        }

        @keyframes Gradient {
            0% {
                background-position: 0% 50%
            }
            50% {
                background-position: 100% 50%
            }
            100% {
                background-position: 0% 50%
            }
        }

        h1,
        h6 {
            font-family: 'Open Sans';
            font-weight: 300;
            text-align: center;
            position: absolute;
            top: 45%;
            right: 0;
            left: 0;
        }

        .backdrop {
            -moz-box-shadow: 0px 6px 5px #111;
            -webkit-box-shadow: 0px 6px 5px #111;
            box-shadow: 0px 2px 10px #111;
            -moz-border-radius:190px;
            -webkit-border-radius:190px;
            border-radius:190px;
        }

        .linktree {
            width: 120px;
            height: 120px;
            background-image: url("/portfolio/assets/img/obi-profile.jpg");
            background-size: cover;
            background-repeat: no-repeat;
            background-position: 50% 50%;
        }

        .btn {
            white-space:normal;
        }
    </style>
</head>

<body>
<!-- Alpha.ly Version 0.1 | Development Version -->
<div class="container">
    <div class="col-xs-12">
        <div class="text-center" style="padding-top: 30px; padding-bottom: 30px;">
            <img class="backdrop linktree">
            <h2 style="color: #ffffff; padding-top: 20px;">Dave Wong</h2>
            <h5 style="color: #ffffff; padding-top: 20px;">Professional Computer Nerd</h5>
        </div>
    </div>
</div>


<div class="container">
    <div class="col-xs-12">
        <div class="text-center">
            @foreach ($links as $link)
                <div style="padding-bottom: 30px;">
                    <button onclick="window.open('{{ $link['url'] }}', '_blank')" type="button" class="btn btn-outline-light" style="width: 80%; padding-top:10px; padding-bottom:10px; font-weight: 800;"><p>{{ $link['site'] }}</p><p>{{ $link['description'] }}</p></button>
                </div>
            @endforeach
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<!-- This pre-lander was created by Brandon Nilsson -->
<!-- You're welcome to edit, reproduce and change this template as long as original contributation stays present on the website at all times.  -->
</body>

</html>