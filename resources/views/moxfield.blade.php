<html>
    <head>
        <title>Moxfield Decks</title>
    </head>
    <body>
        @foreach ($decks as $deck)
            <p><a href="{{ $deck['url'] }}">{{ $deck['name'] }}</a></p>
            <p>{{ ucfirst($deck['format']) }}</p>
            <img src="{{ asset("storage/{$deck['main_card_id']}.jpg")  }}"/>
        @endforeach
    </body>
</html>