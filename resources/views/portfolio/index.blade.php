<!DOCTYPE html>
<html lang="en">

<head>

    <!--░█████╗░██████╗░██╗░██╗░░░░░░░██╗░█████╗░███╗░░██╗░██████╗░░░░░█████╗░░█████╗░███╗░░░███╗-->
    <!--██╔══██╗██╔══██╗██║░██║░░██╗░░██║██╔══██╗████╗░██║██╔════╝░░░░██╔══██╗██╔══██╗████╗░████║-->
    <!--██║░░██║██████╦╝██║░╚██╗████╗██╔╝██║░░██║██╔██╗██║██║░░██╗░░░░██║░░╚═╝██║░░██║██╔████╔██║-->
    <!--██║░░██║██╔══██╗██║░░████╔═████║░██║░░██║██║╚████║██║░░╚██╗░░░██║░░██╗██║░░██║██║╚██╔╝██║-->
    <!--╚█████╔╝██████╦╝██║░░╚██╔╝░╚██╔╝░╚█████╔╝██║░╚███║╚██████╔╝██╗╚█████╔╝╚█████╔╝██║░╚═╝░██║-->
    <!--░╚════╝░╚═════╝░╚═╝░░░╚═╝░░░╚═╝░░░╚════╝░╚═╝░░╚══╝░╚═════╝░╚═╝░╚════╝░░╚════╝░╚═╝░░░░░╚═╝-->

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>obiwong - Professional Computer Nerd</title>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@itsobiwong" />

    <meta property="og:url" content="https://obiwong.com" />
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="obiwong.com: Professional Computer Nerd" />
    <meta property="og:description" content="Personal home page for Obi Wong" />
    <meta property="og:image" content="https://obiwong.com/og-obi-1200.jpg" />
    
    <!-- Favicons -->
    <link href="/favicon.ico" rel="icon">
    <link href="/apple-touch-icon.png" rel="apple-touch-icon">

    <script>
        let start = Date.now();
    </script>

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Sanchez&display=swap" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="/portfolio/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/portfolio/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="/portfolio/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="/portfolio/assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="/portfolio/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/portfolio/assets/vendor/aos/aos.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="/portfolio/assets/css/style.css?{{ $styleCss }}" rel="stylesheet">
    <link href="/portfolio/assets/css/{{ $theme['css'] }}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>

    <!-- =======================================================
    * Template Name: iPortfolio - v1.5.0
    * Template URL: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Mobile nav toggle button ======= -->
<button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>

@include('portfolio.includes.header', ['theme' => $theme])

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="hero-container my-auto animate__animated" data-aos="fade-in">
        <h1 class="my-auto">obiwong</h1>
        <p class="my-auto"><span class="typed" data-typed-items="Professional Computer Nerd, Señor Engineer, Pancake Stack Developer, {{ $theme['tag'] }}"></span></p>
    </div>
</section><!-- End Hero -->

<main id="main">
    @include('portfolio.sections.about', ['tweets' => $tweets, 'favourites' => $favourites])

    @include('portfolio.sections.resume', ['projects' => $projects])

    @include('portfolio.sections.facts')

    @include('portfolio.sections.portfolio', ['decks' => $decks])

    @include('portfolio.sections.spotify', ['topTracks' => $topTracks])
</main><!-- End #main -->

@include('portfolio.includes.footer')

{{--<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>--}}

<!-- Vendor JS Files -->
<script src="{{ asset('/portfolio/assets/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('/portfolio/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('/portfolio/assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
<script src="{{ asset('/portfolio/assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('/portfolio/assets/vendor/counterup/counterup.min.js') }}"></script>
<script src="{{ asset('/portfolio/assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('/portfolio/assets/vendor/venobox/venobox.min.js') }}"></script>
<script src="{{ asset('/portfolio/assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('/portfolio/assets/vendor/typed.js/typed.min.js') }}"></script>
<script src="{{ asset('/portfolio/assets/vendor/aos/aos.js') }}"></script>

<!-- Template Main JS File -->
<script src="/portfolio/assets/js/main.js?{{ $mainJs }}"></script>

<script>
    /**
     * Coin flip profile picture
     */
    var pictures = [
        'dave-profile.jpg',
        '{{ $theme["profile"] }}'
    ];
</script>
<script src="/portfolio/assets/js/script.js?{{ $scriptJs }}"></script>

<script>
    $(document).ready(function() {
        $.ajax({
            'url': '/api/location/{{ $location }}',
            'success': function (res) {
                if (res.city) {
                    $('#network').html("You're on the " + res.isp + ' network in ' + res.city + ', ' + res.country + ' ' + res.emoji);
                    $('#network').show();

                    if (res.weather) {
                        $('#weather').html(res.weather);
                        $('#weather').show();
                    }

                    $('#powered').show();
                }
            }
        });

        console.log(`%c
███████╗ ██████╗ ██████╗ ████████╗██╗   ██╗███╗   ██╗███████╗
██╔════╝██╔═══██╗██╔══██╗╚══██╔══╝██║   ██║████╗  ██║██╔════╝
█████╗  ██║   ██║██████╔╝   ██║   ██║   ██║██╔██╗ ██║█████╗
██╔══╝  ██║   ██║██╔══██╗   ██║   ██║   ██║██║╚██╗██║██╔══╝
██║     ╚██████╔╝██║  ██║   ██║   ╚██████╔╝██║ ╚████║███████╗
╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═══╝╚══════╝
        `, 'color: #a2f274');
        console.log(`{!! $favourites->getQuote() !!}`)
        console.log('%cLucky numbers: %c{{ $favourites->getLuckyNumbers() }}', 'color: #fa1414', 'color: black')
    });
</script>

</body>

</html>