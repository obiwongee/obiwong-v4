<!-- ======= About Section ======= -->
<section id="about" class="about">
    <div class="container">

        <div class="section-title">
            <h2>readme</h2>
        </div>

        <div class="row">
            <div class="col-lg-12" data-aos="fade-left">
                <p>Well, we've made it to version 4 of the website now. Finally decided to stop making things from scratch for my side projects 🤣. Laravel for the backend and a Bootstrap template for the frontend. Some data pulled from the TFT stats from the Riot API, MTG decks from the Moxfield API and tweets from the Twitter API.</p>
                <br/>
                <p>My other notable project is <a href="https://fortressmaximus.io">fortressmaxmius.io</a>. A site for now <a href="https://twitter.com/TransformersTCG/status/1285215953014251520">defunct Transformer TCG</a> from Wizards of the Coast. Another Laravel project which started as a searchable database for all the cards in the game, grew to a deck builder and eventually I started creating content for the community as well. Tried to generate some revenue from ads and affiliate links to buy cards from online retailers, worked with writers and artists to create strategy articles for the game. Tech stack used Redis for cache, Algolia for search and deployed on Digital Ocean.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4" data-aos="fade-right">
                <picture>
                    <source srcset="/portfolio/assets/img/dave-about.webp" type="image/webp">
                    <source srcset="/portfolio/assets/img/dave-about.jpg" type="image/jpeg">
                    <img class="img-fluid" src="/portfolio/assets/img/dave-about.jpg"/>
                </picture>
                <p class="font-italic text-center" style="font-size: 8px;">Maracas Bay, Trinidad and Tobago 🇹🇹</p>
            </div>
            <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">
                <h3>🎶 These are a few of my favourite things</h3>
                <p class="font-italic">
                    If we're ever on a game show and you have to list off my favourite things, here they are in digestible point form.
                </p>
                <div class="row">
                    <div class="col-lg-6">
                        <ul>
                            <li><i class="icofont-rounded-right"></i> <strong>Food:</strong> {{ $favourites->getFood() }}</li>
                            <li><i class="icofont-rounded-right"></i> <strong>Drink:</strong> {{ $favourites->getDrink() }}</li>
                            <li><i class="icofont-rounded-right"></i> <strong>Doritos:</strong> Cool Ranch</li>
                            <li><i class="icofont-rounded-right"></i> <strong>Donut:</strong> Cinnamon Tiny Toms</li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul>
                            <?php $song = $favourites->getSong(); ?>
                            <li><i class="icofont-rounded-right"></i> <strong>Song:</strong> <a class='video-btn' href='#' data-src='{{ $song["youtube"] }}'>{{ $song['title'] }}</a></li>
                            <li><i class="icofont-rounded-right"></i> <strong>Movie:</strong> {{ $favourites->getMovie() }}</li>
                            <li><i class="icofont-rounded-right"></i> <strong>TV Show:</strong> {{ $favourites->getTvShow() }}</li>
                            <li><i class="icofont-rounded-right"></i> <strong>Game:</strong> {{ $favourites->getGame() }}</li>
                        </ul>
                    </div>
                </div>
                <p>
                    What is there to know about Dave? Video games and coding. That's basically it. One of my earliest memories is "playing" Flight Simulator on my dad's old monochrome screen PC. The first time we got a CGA monitor my mind was blown and ever since I started playing the first Civilization I've been forever hooked on video games.
                </p>
                <br/>
                <p>
                    My dad taught me Basic and I guess I thought that was so cool I'd focus the rest of my life on writing code. From DOS batch files to Java, I've finally found a home with PHP.
                </p>
            </div>
        </div>

        <hr/>
        
        <!--<div class="row">
            <div class="col-lg-12 content twitter-container" data-aos="fade-up">
                <h3>
                    <picture>
                        <source srcset="{{ asset('portfolio/assets/img/twitter-icon.webp') }}" type="image/webp">
                        <source srcset="{{ asset('portfolio/assets/img/twitter-icon.png') }}" type="image/png">
                        <img src="{{ asset('portfolio/assets/img/twitter-icon.png') }}" class="game-icon"/>
                    </picture> Recent Tweets
                </h3>
                <div class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php $first = true; ?>
                        @foreach ($tweets as $tweet)
                            @if ($first)
                                <div class="carousel-item active">
                                <?php $first = false; ?>
                            @else
                                <div class="carousel-item">
                            @endif
                                <blockquote class="brooks">
                                    {!! stripslashes($tweet['content']) !!}
                                </blockquote>
                                <small><a href="{{ $tweet['url'] }}">{{ $tweet['ago'] }}</a></small>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>-->

    </div>
</section><!-- End About Section -->

<!-- Modal -->
<div class="video-wrap">
    <div class="video">
        <div class="video-close"><i class="icofont-ui-close"></i></div>
        <iframe width="600" height="340" frameborder="0" gesture="media" allowfullscreen></iframe>
    </div>
</div>