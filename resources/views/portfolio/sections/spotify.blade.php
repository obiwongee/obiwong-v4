<section id="spotify" class="spotify">
    <div class="container">

        <div class="section-title">
            <h2>
                <picture class="img-fluid">
                    <source srcset="{{ asset('portfolio/assets/img/spotify-icon.webp') }}" type="image/webp">
                    <source srcset="{{ asset('portfolio/assets/img/spotify-icon.png') }}" type="image/png">
                    <img class="game-icon" src="{{ asset('portfolio/assets/img/spotify-icon.png') }}"/>
                </picture> Spotify
            </h2>
        </div>

        <div class="row">
            <div class="col-lg-12" data-aos="fade-down">
                <p>My top 10 tracks from the last four weeks pulled from the Spotify API.</p>
            </div>
        </div>

        <div data-aos="fade-up">
            <?php $count=0; ?>
            @foreach($topTracks as $track)
                <div class="row m-2 {{ $count % 2 ? 'spotify-alt' : '' }} p-2">
                    <div class="col-lg-1 d-flex text-center">
                        <div class="my-auto spotify-track-placement">
                            {{ ++$count }}
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <img class="spotify-album" data-src="{{ $track['album_art'] }}"/>
                    </div>
                    <div class="col-lg-9 d-flex spotify-track-info">
                        <div class="my-auto">
                            <p>{{ $track['artist'] }}</p>
                            <p><a href="{{ $track['url'] }}">{{ $track['title'] }}</a></p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>