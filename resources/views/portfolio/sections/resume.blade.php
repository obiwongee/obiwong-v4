<!-- ======= Resume Section ======= -->
<section id="resume" class="resume section-bg">
    <div class="container">

        <div class="section-title">
            <h2>Projects</h2>
        </div>

        <div class="row">
            <p style="margin: 10px;">
                Side projects and fun proof of concepts I work on from time to time. Always trying to fulfill the prophecy of buying a domain and actually using it 🤣
            </p>
        </div>

        <div class="row">
            <table class="table">
                <tbody>
                    @foreach($projects as $project)
                        <tr>
                            <td>{{ $project['name'] }}</td>
                            <td><a href="{{ $project['link'] }}" target="_blank">{{ $project['link'] }}</a></td>
                            <td>{{ $project['description'] }}</td>
                        </tr>
                        @if (array_key_exists('source', $project))
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td colspan="2">
                                Source: <a href="{{ $project['source'] }}">{{ $project['source'] }}</a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</section><!-- End Resume Section -->