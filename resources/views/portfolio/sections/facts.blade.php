<!-- ======= Facts Section ======= -->
<section id="facts" class="facts">
    <div class="container">

        <div class="section-title">
            <h2>
                <picture class="img-fluid">
{{--                    <source srcset="{{ asset('portfolio/assets/img/tft-icon.webp') }}" type="image/webp">--}}
                    <source srcset="{{ asset('portfolio/assets/img/snap.png') }}" type="image/png">
                    <img class="game-icon" src="{{ asset('portfolio/assets/img/tft-icon.png') }}"/>
                </picture> Marvel Snap
            </h2>
        </div>

        <div class="row">
            <div class="col-lg-12" data-aos="fade-down">
                <p>I never thought I would ever find a game that could replace <a href="https://overpower.ca/">Marvel Overpower</a>. Marvel Snap is everything I look for in a card game. It's a quick, fast paced card game where you get to build a team of your favourite Marvel heroes and villians. The location system keeps things fresh and the small deck size makes it easy to just brew and tune decks. It's a refreshing take on card games where every new game is trying to be the next Hearthstone.</p>
                <p>Profile @ <a href="https://snap.fan/p/9d00126f-d61b-447e-bb65-6fe27329545b/stats/">snap.fan</a></p>
                <span style="font-size: 8px">Stats icons by <a href="https://www.flaticon.com/authors/kawaii/flat?author_id=1&type=standard">Freepik</a></span>
            </div>
        </div>

        <hr/>

        <h3 data-aos="fade-right">Stats</h3>

        <div class="row no-gutters">

            <div class="col-lg-2 col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up" data-aos-delay="300">
                <div class="count-box">
                    <img class="ico" src="{{ asset("portfolio/assets/img/planner.png") }}"/>
                    <span data-toggle="counter-up">{{ $snapStats->collection_level }}</span>
                    <p><strong>Collection Level</strong></p>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up" data-aos-delay="300">
                <div class="count-box">
                    <img class="ico" src="{{ asset("portfolio/assets/img/diamond.png") }}"/>
                    <span data-toggle="counter-up">{{ $snapStats->rank }}</span>
                    <p><strong>Rank</strong></p>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up">
                <div class="count-box">
                    <img class="ico" src="{{ asset("portfolio/assets/img/video-game.png") }}"/>
                    <span data-toggle="counter-up">{{ $snapStats->games_played }}</span>
                    <p><strong>Games Played</strong></p>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="count-box">
                    <img class="ico" src="{{ asset("portfolio/assets/img/podium.png") }}"/>
                    <span data-toggle="counter-up">{{ $snapStats->games_won }}</span>
                    <p><strong>Wins</strong></p>
                </div>
            </div>

            <div class="col-lg-2 col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up" data-aos-delay="200">
                <div class="count-box">
                    <img class="ico" src="{{ asset("portfolio/assets/img/game-over.png") }}"/>
                    <span data-toggle="counter-up">{{ $snapStats->games_conceded }}</span>
                    <p><strong>Games Conceded</strong></p>
                </div>
            </div>

        </div>

{{--        <hr/>--}}

{{--        <div class="row" >--}}
{{--            <div class="col-lg-12 content" data-aos="fade-left">--}}
{{--                <h3>Recent Matches</h3>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div data-aos="fade-right">--}}
{{--            @foreach($lastMatches as $lastMatch)--}}
{{--                <?php $tftData->setSet($lastMatch->set); ?>--}}
{{--                <div class="row m-5" >--}}
{{--                    <div class="col-lg-1 d-flex">--}}
{{--                        <span class="tft-ordinal-placement my-auto">{{ $lastMatch->getOrdinalPlacement() }}</span>--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-11">--}}
{{--                        <div>--}}
{{--                            @foreach ($lastMatch->getSortedUnits('tier')->reverse() as $unit)--}}
{{--                                <div class="tft-unit">--}}
{{--                                    <img class="{{ array_key_exists('chosen', $unit) ? 'tft-unit-chosen' : '' }}" src="{{ $unit['asset']  }}" data-toggle="tooltip" data-placement="top" title="{{ $tftData->getChampionName($unit['character_id']) }}"/>--}}
{{--                                    <div class="tft-unit-stars">--}}
{{--                                        {{ str_repeat('⭐️', $unit['tier']) }}--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                        <div>--}}
{{--                            @foreach ($lastMatch->getSortedTraits('num_units')->reverse() as $trait)--}}
{{--                                @if ($trait['tier_current'] > 0)--}}
{{--                                    <div class="tft-trait" title="{{ $tftData->getTraitDescription($trait['name']) }}" data-toggle="tooltip" data-placement="bottom" data-custom-class="tooltip-trait">--}}
{{--                                        <img class="{{ $trait['name'] == $lastMatch->getChosen() ? 'tft-trait-chosen' : '' }}" src="{{ $trait['asset']  }}" alt="{{ $trait['name'] }}"/>--}}
{{--                                        <span>{{ $trait['num_units'] }}</span>--}}
{{--                                    </div>--}}
{{--                                @endif--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @endforeach--}}
{{--        </div>--}}
    </div>
</section><!-- End Facts Section -->