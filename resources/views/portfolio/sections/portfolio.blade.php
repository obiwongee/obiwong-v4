<!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="portfolio section-bg">
    <div class="container">

        <div class="section-title">
            <h2>
                <picture class="img-fluid">
                    <source srcset="{{ asset('portfolio/assets/img/mtg-icon.webp') }}" type="image/webp">
                    <source srcset="{{ asset('portfolio/assets/img/mtg-icon.png') }}" type="image/png">
                    <img class="game-icon" src="{{ asset('portfolio/assets/img/mtg-icon.png') }}"/>
                </picture> Magic the Gathering
            </h2>
        </div>

        <div class="row">
            <div class="col-lg-12" data-aos="fade-down">
                <p>I've been playing Magic since 94/95 started around Revised/Ice Age era. Especially after listening to Maro's <a href="https://magic.wizards.com/en/articles/media/podcasts">Drive to Work podcast</a> (if you have any interest in game design I highly recommend it), I really do think Magic The Gathering is probably one of the greatest games ever made.</p>
                <p>My MTG pet project is my <a href="https://mtgbattlebox.com/rules/">Battle Box</a> which can be found here <a href="https://cubecobra.com/cube/list/x1r">Obi's Battle Box</a>.</p>
                <p>Decks pulled from <a href="https://www.moxfield.com/">moxfield.com</a>. Deck images pulled from their server and stored locally. Updated daily.</p>
                <p>Profile @ <a href="https://www.moxfield.com/users/itsobiwong">moxfield.com</a></p>
            </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="100">

            @foreach ($decks as $deck)
                <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                    <div class="portfolio-wrap">
                        <p class="deck-name">{{ ucfirst($deck->format) }} - {{ $deck->name }}</p>
                        <picture>
                            <source class="img-fluid" srcset="{{ asset("storage/{$deck['main_card_id']}.webp") }}" type="image/webp">
                            <source class="img-fluid" srcset="{{ asset("storage/{$deck['main_card_id']}.jpg") }}" type="image/jpeg">

                            <img src="{{ asset("storage/{$deck['main_card_id']}.jpg")  }}" class="img-fluid" alt="">
                        </picture>
                        <div class="portfolio-links">
                            <a href="{{ $deck->url }}" title="More Details" target="_blank"><i class="bx bx-link"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</section><!-- End Portfolio Section -->