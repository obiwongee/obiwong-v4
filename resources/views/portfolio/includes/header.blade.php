<!-- ======= Header ======= -->
<header id="header">
    <div class="d-flex flex-column">

        <div class="profile">
            <img id="profile" src="/portfolio/assets/img/{{ $theme['profile'] }}" alt="" class="img-fluid rounded-circle animate__animated">
            <h1 class="text-light text-center"><a href="https://obiwong.com">Dave Wong</a></h1>
            <div id="current-song-container" class="text-center">
                <span><i class='bx bxs-music current-song-emote'></i> </span><span id="current-song"></span><span> <i class='bx bxs-music current-song-emote'></i></span>
            </div>
            <div class="social-links mt-3 text-center">
                <a href="https://www.threads.net/@itsobiwong" class="threads" target="_blank"><i class='bx bxs-message-square-dots'></i></a>
                <a href="https://facebook.com/obiwong" class="facebook" target="_blank"><i class="bx bxl-facebook"></i></a>
                <a href="https://www.instagram.com/smomochi" class="instagram" target="_blank"><i class="bx bxl-instagram"></i></a>
                <a href="https://open.spotify.com/playlist/7wfqIkHkiDHBk51W2iosCv?si=5qijgIZLS1edv24lhXJB7g" class="spotify" target="_blank"><i class='bx bxl-spotify'></i></a>
            </div>
        </div>

        <nav class="nav-menu">
            <ul>
                <li class="active"><a href="#hero"><i class="bx bx-home"></i> <span>Home</span></a></li>
                <li><a href="#about"><i class="bx bx-user"></i> <span>About</span></a></li>
                <li><a href="#resume"><i class="bx bx-file-blank"></i> <span>Projects</span></a></li>
                <li><a href="#facts"><i class="bx bxs-cube"></i> Marvel Snap</a></li>
                <li><a href="#portfolio"><i class="bx bx-meteor"></i> Magic the Gathering</a></li>
                <li><a href="#spotify"><i class='bx bx-play-circle nav-spotify'></i> Spotify</a></li>
            </ul>
        </nav><!-- .nav-menu -->
        <button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>

    </div>
</header><!-- End Header -->