<html>
    <head>
        <title>Mangz Fightcade</title>

        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container" style="margin-bottom: 100px;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-center">How to Fightcade</h1>
                    <ol>
                        <li>Download Fightcade 2: <a href="https://www.fightcade.com/" target="_blank">Fightcade 2</a></li>
                        <li>Create an account for Fightcade and login</li>
                        <li>Install fightcade. It's important to note where you installed it</li>
                        <li>Download auto-downloader configuration: <a href="{{ config('app.url') }}/download/fc2json.zip">fc2json.zip</a></li>
                        <li>Unzip the contents of jc2json.zip to the emulator folder of where you installed Fightcade 2</li>

                        <img src="{{ asset('/storage/fightcade/fightcade.png') }}" style="width: 100%;"/>

                        <li>Start up Fightcade and join a room. When you join a room the ROM should autodownload</li>
                        <li>To test your controls and make sure the ROM was downloaded you can hit the "test game" button in the top right</li>
                        <li>Join the same room as another Mangz, send a challenge request and play gaemz</li>
                        <li>For Fightcade help see their help page: <a href="https://www.fightcade.com/help" target="_blank">Fightcade Help</a></li>
                    </ol>
                    <h5>Set up Daytona USA</h5>
                    <p>The Daytona USA ROM won't auto-download so you have to set it up manually</p>
                    <ol>
                        <li>Go to <a href="https://edgeemu.net/details-4787.htm">https://edgeemu.net/details-4787.htm</a> and download the chd file</li>
                        <li>Download the file to where you installed Fightcade in the "emulator/flycast/ROMs" folder</li>
                    </ol>
                    <div class="text-center">
                        <img src="{{ asset('/storage/fightcade/ryu-ken-cvs-specialintro.gif') }}"/>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
