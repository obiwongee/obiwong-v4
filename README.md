# obiwong.com

![](https://obiwong.com/portfolio/assets/img/obi-profile.jpg)

## Requirements

* PHP 7.4
* Laravel 8
* Redis
* MySql

## Setup

### Larasail

A lot of the magic for deploying on Digital Ocean was done with [LaraSail](https://www.digitalocean.com/community/tools/larasail)

### Link Storage
* create the directory `<project path>/storage/app/public`
* run `php artisan storage:link` to create a symlink of `storage` in `public`

### Create Framework Directories
```
cd storage/
mkdir -p framework/{sessions,views,cache}
chown -R www-data:www-data framework
```

## PHP Dependencies

Aside from the PHP dependencies required for Laravel you'll need the following:

* php7.4-common 
* php7.4-bcmath 
* php7.4-json
* php7.4-mbstring 
* php7.4-mysql 
* php7.4-curl
* php7.4-intl
* php7.4-redis

## Other Apt Dependencies  
* openssl 

## API Keys 
* [Riot](https://developer.riotgames.com/docs/portal)
* [Twitter](https://developer.twitter.com/en/docs)
* [Spotify](https://developer.spotify.com/documentation/web-api/)
* [ipgeolocation](https://ipgeolocation.io/)
* [openweather](https://openweathermap.org/api)

```
#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NOCOLOR='\033[0m'

echo -e "${NOCOLOR}Pulling master"
echo ""
git -C /home/obiwong/obiwong-v4 pull origin master;

if [ $? -ne 0 ]; then
	echo ""
	echo -e "${RED}Failed to pull origin master";
	exit 1;
fi

echo ""
echo -e "${GREEN}Git pull success"
echo ""

echo -e "${NOCOLOR}Install Composer dependencies"
echo ""

composer install -d /home/obiwong/obiwong-v4 

if [ $? -ne 0 ]; then
        echo ""
        echo -e "${RED}Failed to install composer dependencies"
        exit 1;
fi

echo ""
echo -e "${GREEN}Installed composer dependencies successfully"
echo "" 

echo -e "${NOCOLOR}Applying DB migrations"
echo ""
php /home/obiwong/obiwong-v4/artisan migrate --force

if [ $? -ne 0 ]; then
	echo ""
        echo -e "${RED}Failed to migrate DB"
        exit 1;
fi

echo ""
echo -e "${GREEN}DB migration success"
echo ""

echo -e "${NOCOLOR}Setting asset cache"
echo ""
php /home/obiwong/obiwong-v4/artisan asset-cache

if [ $? -ne 0 ]; then
	echo ""
        echo -e "${RED}Failed to set asset cache"
        exit 1;
fi

echo -e "${GREEN}Asset cache success"
echo ""

echo ""
echo -e "${NOCOLOR}Checking HTTP status code"
echo ""

STATUS=`curl -s -o /dev/null -w "%{http_code}" https://obiwong.com`

if [ $STATUS -ne 200 ]; then
        echo ""
        echo -e "${RED} HTTP check failed with status code ${STATUS}"
        exit 1;
fi

echo -e "${GREEN}${STATUS} OK!"
echo ""

echo -e "${GREEN}Deployment successful"
echo ""

exit 0
```