<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->id();
            $table->string('match_id', 14);
            $table->unsignedBigInteger('game_datetime');
            $table->float('game_length');
            $table->json('traits');
            $table->json('units');
            $table->integer('players_eliminated');
            $table->integer('total_damage_to_players');
            $table->integer('placement');
            $table->timestamps();

            $table->unique('match_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
