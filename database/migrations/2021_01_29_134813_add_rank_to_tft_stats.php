<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRankToTftStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tft_stats', function (Blueprint $table) {
            $table->string('tier')->after('players_eliminated')->nullable();
            $table->string('rank')->after('tier')->nullable();
            $table->unsignedInteger('points')->after('rank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tft_stats', function (Blueprint $table) {
            $table->dropColumn('tier');
            $table->dropColumn('rank');
            $table->dropColumn('points');
        });
    }
}
