<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTftStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tft_stats', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('time_played');
            $table->float('average_placement');
            $table->unsignedInteger('wins');
            $table->unsignedInteger('top_four');
            $table->unsignedInteger('players_eliminated');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tft_stats');
    }
}
