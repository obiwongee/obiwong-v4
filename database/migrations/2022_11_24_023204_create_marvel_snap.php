<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarvelSnap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marvel_snap', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('collection_level');
            $table->integer('games_played');
            $table->integer('games_won');
            $table->integer('games_conceded');
            $table->integer('games_opponent_conceded');
            $table->integer('rank');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marvel_snap');
    }
}
