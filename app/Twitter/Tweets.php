<?php

namespace App\Twitter;

use RuntimeException;
use Thujohn\Twitter\Facades\Twitter;
use Illuminate\Support\Facades\Cache;

class Tweets
{
    public static function getTweets(): array
    {
        try {
            if ($timelineData = Cache::get('twitter')) {
                $tweets = json_decode($timelineData);
            } else {
                $timelineData = Twitter::getUserTimeline(['screen_name' => 'itsobiwong', 'count' => 20, 'format' => 'json']);

                Cache::put('twitter', $timelineData, now()->addDay());

                $tweets = json_decode($timelineData);
            }
        } catch (RunTimeException $e) {
            \Log::error(__METHOD__ . ' RunTimeException ' . $e->getMessage());

            $tweets = [];
        }

        return self::formatTweets($tweets);
    }

    private static function formatTweets(array $tweets): array
    {
        $data = [];

        foreach ($tweets as $tweet) {
            if (is_null($tweet->in_reply_to_status_id) && !$tweet->retweeted && !$tweet->is_quote_status) {
                $data[] = [
                    'content' => Twitter::linkify($tweet),
                    'ago' => Twitter::ago($tweet->created_at),
                    'url' => Twitter::linkTweet($tweet),
                ];
            }
        }

        return $data;
    }
}