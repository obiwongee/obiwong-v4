<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class TFTData
{
    private $champions;
    private $traits;

    public function __construct(string $set = 'set4')
    {
        $set = 'set4';
        
        $this->setSet($set);
    }

    public function setSet(string $set)
    {
        $set = strtolower($set);

        $championData = Cache::get("champion-data-{$set}");
        if (!$championData) {
            $championData = file_get_contents(public_path() . "/tft/{$set}/champions.json");
            Cache::put("champion-data-{$set}", $championData, now()->addMonth());
        }

        $traitData = Cache::get("trait-data-{$set}");
        if (!$traitData) {
            $traitData = file_get_contents(public_path() . "/tft/{$set}/traits.json");
            Cache::put("trait-data-{$set}", $traitData, now()->addMonth());
        }

        $this->champions = collect(json_decode($championData, true))->keyBy('championId');
        $this->traits = collect(json_decode($traitData, true))->keyBy('key');
    }

    public function getChampionName(string $championId): string
    {
        if ($this->champions->has($championId)) {
            return $this->champions->get($championId)['name'];
        }

        return $championId;
    }

    public function getTraitName(string $traitId): string
    {
        if ($this->traits->has($traitId)) {
            $trait = $this->traits->get($traitId);

            return $trait['name'];
        }

        return $traitId;
    }

    public function getTraitDescription(string $traitId): string
    {
        if ($this->traits->has($traitId)) {
            $trait = $this->traits->get($traitId);

            return $trait['name'] . ' - ' . $trait['description'];
        }

        if ($this->traits->has('Set4_' . $traitId)) {
            $trait = $this->traits->get('Set4_' . $traitId);

            return $trait['name'] . ' - ' . $trait['description'];
        }

        if ($this->traits->has('Set5_' . $traitId)) {
            $trait = $this->traits->get('Set5_' . $traitId);

            return $trait['name'] . ' - ' . $trait['description'];
        }

        return $traitId;
    }
}