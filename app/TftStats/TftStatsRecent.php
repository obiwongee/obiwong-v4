<?php

namespace App\TftStats;

use Illuminate\Database\Eloquent\Model;

class TftStatsRecent extends Model
{
    protected $table = 'tft_stats_recent';
}