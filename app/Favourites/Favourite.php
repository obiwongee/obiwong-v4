<?php

namespace App\Favourites;

class Favourite
{
    private $foods = [
        'Mashed Potatoes',
        'Garlic Bread',
        '감자탕'
    ];

    private $songs = [
        [
            'title' => 'Wham! - Careless Whispers',
            'youtube' => 'https://www.youtube.com/embed/izGwDsrQ1eQ',
        ],
        [
            'title' => 'Donna Lewis - I Love You Always Forever',
            'youtube' => 'https://www.youtube.com/embed/SqdWTeXWvOg'
        ],
        [
            'title' => 'GEM Tang - 泡沫',
            'youtube' => 'https://www.youtube.com/embed/GHXr4bBxHCo'
        ],
        [
            'title' => '田馥甄 - 寂寞寂寞就好',
            'youtube' => 'https://www.youtube.com/embed/DyFIzKYQQYE'
        ],
        [
            'title' => '彭羚 - 讓我跟你走',
            'youtube' => 'https://www.youtube.com/embed/zMxQXPsRWUE'
        ],
        [
            'title' => '周杰伦 - 可愛女人',
            'youtube' => 'https://www.youtube.com/embed/87VUC4J_0Ps'
        ],
        [
            'title' => '容祖兒 - 痛愛',
            'youtube' => 'https://www.youtube.com/embed/ZMUAQQbfuJk?start=527&end=875'
        ],
        [
            'title' => '容祖兒 - 習慣失戀',
            'youtube' => 'https://www.youtube.com/embed/ZMUAQQbfuJk?start=256&end=526'
        ],
        [
            'title' => 'Echosmith - Cool Kids',
            'youtube' => 'https://www.youtube.com/embed/SSCzDykng4g'
        ],
        [
            'title' => 'Yellowcard - Only One',
            'youtube' => 'https://www.youtube.com/embed/RJLkcPhVi9w',
        ],
        [
            'title' => 'Ailee - Rainy Day',
            'youtube' => 'https://www.youtube.com/embed/4VvC1_81aHg',
        ],
        [
            'title' => 'Ciara - Never Ever',
            'youtube' => 'https://www.youtube.com/embed/R7dDzeVNjRY?start=40&end=',
        ],
        [
            'title' => 'Mariah Carey - Always Be My Baby',
            'youtube' => 'https://www.youtube.com/embed/LfRNRymrv9k',
        ],
        [
            'title' => 'Usher - You Make Me Wanna',
            'youtube' => 'https://www.youtube.com/embed/bQRzrnH6_HY',
        ],
        [
            'title' => 'Zaid Tabani - Feels Like 1994',
            'youtube' => 'https://www.youtube.com/embed/KB-0jOt3NrQ',
        ],
        [
            'title' => 'Noreaga - Superthug',
            'youtube' => 'https://www.youtube.com/embed/gs9ngd-uq6I?start=39&end=',
        ],
        [
            'title' => '楊千嬅 - 陌路',
            'youtube' => 'https://www.youtube.com/embed/pxPIne8Ew80',
        ],
        [
            'title' => 'Paramore - Last Hope',
            'youtube' => 'https://www.youtube.com/embed/XoYu7K6Ywkg',
        ],
    ];

    private $movies = [
        'The Matrix',
        'Serendipity',
        'Young and Dangerous',
        'Transformers: The Movie (1986)',
        'Lord of the Rings',
        'Star Wars',
    ];

    private $tvShows = [
        'LOST',
        'Clone Wars (2003)',
        'Star Trek: The Next Generation',
        'Transformers (1984)',
        'X-Men (1992)',
        'Game of Thrones',
        '30 Rock',
        'Community',
        'The Newsroom',
        'The Bear',
    ];

    private $drinks = [
        'Coffee',
        'HK Cold Lemon Tea',
        'HK Cold Milk Tea',
        'Bubble Tea',
        'Macallan',
    ];

    private $games = [
        'World of Warcraft',
        'Civilization',
        'Super Street Fighter II Turbo',
        'Street Fighter Alpha 2',
        'Magic the Gathering',
        'Team Fight Tactics',
        'Sim City 4',
        'X-Men vs Street Fighter',
        'Marvel vs Capcom',
        'Marvel vs Capcom 2',
        'Final Fantasy VI',
        'Final Fantasy VII',
        'Splendor',
        'Revolution',
        'Stellaris',
    ];

    private $quotes = [
        "More is lost by indecision than wrong decision.",
        "Worrying is like paying interest on a debt you may never owe.",
        "Instead of buying your children all the things you never had, you should teach them all the things you were never taught. Material wears out but knowledge stays.",
        "Beware of destination addiction…until you give up the idea that happiness is somewhere else, it will never be where you are.",
        "As long as you're learning, you're not failing.",
        "It's only a mistake if you do it again.",
        "Ever love someone so much you would do anything for them? Yeah, well, make that someone yourself and do whatever the hell you want.",
        "It's better try and to fail than to ever even try at all.",
        "You can't go back and change the beginning, but you can start where you are and change the ending.",
        "It's okay if you fall apart sometimes. Taco's fall apart and we still love them.",
        "Don't measure your life with someone else's ruler.",
        "If I do a job in 30 minutes it's because I spent 10 years learning how to do it in 30 minutes. You owe me for the years, not the minutes.",
        "Just like in poker, in life you have to play the cards you're dealt.",
        "You teach people how to treat you.",
        "One of the great activities is skateboarding. To learn to do a skateboard trick, how many times you got to get something wrong until you get it right...And you hurt yourself and you learn that trick, now you got a life lesson. Whenever I see those skateboard kids, I think, 'Those skateboard kids will be all right'",
        "No one is trying to fix problems anymore, everyone is trying to make enough money so the problems don't apply to them."
    ];

    public function getFood(): string
    {
        return collect($this->foods)->random();
    }

    public function getSong(): array
    {
        return collect($this->songs)->random();
    }

    public function getMovie(): string
    {
        return collect($this->movies)->random();
    }

    public function getTvShow(): string
    {
        return collect($this->tvShows)->random();
    }

    public function getDrink(): string
    {
        return collect($this->drinks)->random();
    }

    public function getGame(): string
    {
        return collect($this->games)->random();
    }

    public function getLuckyNumbers(): string
    {
        $numbers = collect([]);

        while ($numbers->count() < 7) {
            $n = rand(1, 49);

            if (!$numbers->contains($n)) {
                $numbers->push($n);
            }
        }

        return implode(' ', $numbers->sort()->toArray());
    }

    public function getQuote(): string
    {
        return collect($this->quotes)->random();
    }
}