<?php

namespace App\Console\Commands;

use App\Clients\Spotify;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class SpotifyRefreshToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spotify:set-refresh-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the refresh token for Spotify';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Spotify $client)
    {
        Cache::put('spotify:refresh-token', config('spotify.refresh_token'));
        
        $client->refreshToken();

        return 0;
    }
}
