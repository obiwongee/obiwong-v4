<?php

namespace App\Console\Commands;

use App\Clients\Riot;
use App\Matches\Match;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class SyncMatches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'riot:sync-matches';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync TFT matches from Riot API';

    private $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Riot $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $matches = collect($this->client->getMatches());
        $chunks = $matches->chunk(30);

        foreach ($chunks as $matchIds) {
            $this->syncMatches($matchIds);
        }

        return 0;
    }

    private function syncMatches(Collection $matchIds)
    {
        foreach ($matchIds as $matchId) {
            $match = Match::firstOrNew(['match_id' => $matchId]);

            if (!$match->exists) {
                $matchData = $this->client->getMatch($matchId);

                $info = $matchData['info'];

                $participants = $info['participants'];
                $me = collect($participants)->filter(function ($data) {
                    return $data['puuid'] == config('riot.puuid');
                })->first();

                $match->game_datetime = $info['game_datetime'];
                $match->game_length = $info['game_length'];
                $match->traits = $me['traits'];
                $match->units = $me['units'];
                $match->players_eliminated = $me['players_eliminated'];
                $match->total_damage_to_players = $me['total_damage_to_players'];
                $match->placement = $me['placement'];
                $match->set = "set{$info['tft_set_number']}";
                $match->save();
            }
        }
    }
}
