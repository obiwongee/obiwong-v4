<?php

namespace App\Console\Commands;

use App\MarvelSnap;
use PHPHtmlParser\Dom;
use Illuminate\Console\Command;
use PHPHtmlParser\Dom\Node\Collection;
use PHPHtmlParser\Dom\Node\HtmlNode;

class SyncSnapFan extends Command
{
    private const SNAPFAN_URL = 'https://snap.fan/p/9d00126f-d61b-447e-bb65-6fe27329545b/stats/';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync-snap-fan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync stats from snap fan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(Dom $parser)
    {
//        $jsonOut = storage_path('current-deck.json');
//        exec("wget --header='User-Agent: Mozilla/5.0 Gecko/2010 Firefox/5' -O $jsonOut https://snap.fan/u/get-current-deck/?api_key=9b1070ce-9593-4d67-9272-6d6c04555a5d", $data);
//        $json = file_get_contents($jsonOut, true);
//        dd($json)l

        $htmlOut = storage_path('snapfan.html');
        exec("wget --header='User-Agent: Mozilla/5.0 Gecko/2010 Firefox/5' -O $htmlOut https://snap.fan/p/9d00126f-d61b-447e-bb65-6fe27329545b/stats/", $data);

        $html = file_get_contents($htmlOut);
        $dom = $this->getDom($html);

        $headers = $dom->find('h3');

        $data = collect();

        $headers->each(function (HtmlNode $node) use ($data) {
            switch ($node->innerhtml) {
                case "Collection Level":
                    $gamesPlayed = $node->parent->find('span')->innerHtml;
                    $data->put('collection_level', $gamesPlayed);
                    break;
                case "Games Played":
                    $gamesPlayed = $node->parent->find('span')->innerHtml;
                    $data->put('games_played', $gamesPlayed);
                    break;
                case "Games Won":
                    $gamesWon = $node->parent->find('span')->innerHtml;
                    preg_match('/^[0-9]*/', $gamesWon, $matches);
                    $data->put('games_won', $matches[0]);
                    break;
                case "Games Conceded":
                    $gamesConceded = $node->parent->find('span')->innerHtml;
                    preg_match('/^[0-9]*/', $gamesConceded, $matches);
                    $data->put('games_conceded', $matches[0]);
                    break;
                case "Games Opponent Conceded":
                    $gamesOpponentsConceded = $node->parent->find('span')->innerHtml;
                    preg_match('/^[0-9]*/', $gamesOpponentsConceded, $matches);
                    $data->put('games_opponent_conceded', $matches[0]);
                    break;
                case "Rank":
                    $rank = $node->parent->find('span')->innerHtml;
                    preg_match('/^[0-9]*/', $rank, $matches);
                    $data->put('rank', $matches[0]);
                    break;
            }
        });

        $marvelSnap = new MarvelSnap($data->toArray());
        $marvelSnap->save();
//        dd($data);

        return 0;
    }

    private function getDom(string $html)
    {
        $dom = app(Dom::class);

        return $dom->loadStr($html);
    }
}
