<?php

namespace App\Console\Commands;

use App\Clients\Moxfield;
use App\Decks\Deck;
use Illuminate\Console\Command;

class SyncDecks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'moxfield:sync-decks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync decks from Moxfield';

    private $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Moxfield $client)
    {
        parent::__construct();

        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $decks = $this->client->getDeckList();

        foreach ($decks['data'] as $data) {
            $this->client->syncCardArt($data['mainCardId']);

            $deck = Deck::firstOrNew(['moxfield_id' => $data['id']]);
            $deck->name = $data['name'];
            $deck->format = $data['format'];
            $deck->url = $data['publicUrl'];
            $deck->main_card_id = $data['mainCardId'];
            $deck->save();
        }

        return 0;
    }
}
