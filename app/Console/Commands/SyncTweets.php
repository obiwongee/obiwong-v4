<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Thujohn\Twitter\Facades\Twitter;

class SyncTweets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:fetch-tweets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Tweets from Twitter feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $timelineData = Twitter::getUserTimeline([
            'screen_name' => config('twitter.screen_name'),
            'count' => config('twitter.fetch_count'),
            'format' => 'json'
        ]);

        Cache::put('twitter', $timelineData);

        return 0;
    }
}
