<?php

namespace App\Console\Commands;

use App\Clients\Riot;
use App\Matches\Match;
use App\TftStats\TftStat;
use App\TftStats\TftStatsRecent;
use Illuminate\Console\Command;

class CalculateTftStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'riot:calculate-stats';

    private $riotClient;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate stats for TFT';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Riot $riotClient)
    {
        parent::__construct();

        $this->riotClient = $riotClient;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $summonerRank = $this->riotClient->getSummonerRank();

        if (empty($summonerRank)) {
            exit(0);
        }

        $matches = Match::orderBy('game_datetime', 'desc')->get();
        $wins = 0;
        $top4 = 0;
        $timePlayed = 0;
        $placements = 0;
        $playersEliminated = 0;

        $recentCount = 0;
        $recentWins = 0;
        $recentTop4 = 0;
        $recentPlacements = 0;

        foreach ($matches as $match) {
            $timePlayed += $match->game_length;
            $placements += $match->placement;
            $playersEliminated += $match->players_eliminated;

            if ($match->placement == 1) {
                $wins++;

                if ($recentCount < 20) {
                    $recentWins++;
                }
            }

            if ($match->placement <= 4) {
                $top4++;

                if ($recentCount < 20) {
                    $recentTop4++;
                }
            }

            if ($recentCount < 20) {
                $recentPlacements += $match->placement;
            }

            $recentCount++;
        }

        $stat = new TftStat();
        $stat->time_played = $timePlayed;
        $stat->average_placement = round($placements / count($matches), 1);
        $stat->wins = $wins;
        $stat->top_four = $top4;
        $stat->players_eliminated = $playersEliminated;
        $stat->tier = $summonerRank[0]['tier'];
        $stat->rank = $summonerRank[0]['rank'];
        $stat->points = $summonerRank[0]['leaguePoints'];

        $stat->save();

        $recentStat = new TftStatsRecent();
        $recentStat->average_placement = round($recentPlacements / 20, 1);
        $recentStat->wins = $recentWins;
        $recentStat->top_four = $recentTop4;

        $recentStat->save();
    }
}
