<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class AssetCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate and cache md5 hashes of assets';

    private $files;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->files = [
            public_path('/portfolio/assets/css/style.css'),
            public_path('/portfolio/assets/css/jace.css'),
            public_path('/portfolio/assets/css/obi.css'),
            public_path('/portfolio/assets/js/script.js'),
            public_path('/portfolio/assets/js/main.js'),
        ];
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach ($this->files as $file) {
            $data = file_get_contents($file);
            Cache::put($file, md5($data));
        }
    }
}
