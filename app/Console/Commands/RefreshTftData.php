<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class RefreshTftData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'riot:refresh-tft-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load TFT json files into cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $championData = file_get_contents(public_path() . '/tft/champions.json');
        Cache::put('champion-data', $championData, now()->addMonth());

        $traitData = file_get_contents(public_path() . '/tft/traits.json');
        Cache::put('trait-data', $traitData, now()->addMonth());

        return 0;
    }
}
