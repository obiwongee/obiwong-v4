<?php

namespace App\Console\Commands;

use App\Clients\Spotify;
use Illuminate\Console\Command;

class FetchTracks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spotify:fetch-tracks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get top tracks from Spotify';

    private $spotify;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Spotify $spotify)
    {
        parent::__construct();

        $this->spotify = $spotify;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->spotify->fetchTracks();

        return 0;
    }
}
