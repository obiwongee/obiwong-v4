<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarvelSnap extends Model
{
    protected $table = 'marvel_snap';

    protected $fillable = [
        'collection_level',
        'games_played',
        'games_won',
        'games_conceded',
        'games_opponent_conceded',
        'rank',
    ];
}