<?php

namespace App\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Storage;

class Moxfield
{
    private $client;
    private $baseUrl;
    private $assetUrl;
    private $assetPrefix;
    private $assetSuffix;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->baseUrl = config('moxfield.api_base_url');
        $this->assetUrl = config('moxfield.asset_base_url');
        $this->assetPrefix = config('moxfield.asset_prefix');
        $this->assetSuffix = config('moxfield.asset_suffix');
    }

    public function getDeckList(): array
    {
        return $this->get('/users/itsobiwong/decks');
    }

    public function syncCardArt(string $cardId)
    {
        if (!Storage::exists("public/$cardId.jpg")) {
            $url = "{$this->assetUrl}{$this->assetPrefix}$cardId{$this->assetSuffix}";

            $image = imagecreatefromjpeg($url);

            imagejpeg($image, storage_path("/app/public/$cardId.jpg"), 50);
            imagewebp($image, storage_path("/app/public/$cardId.webp"), 50);
        }
    }

    private function get(string $url): array
    {
        try {
            $res = $this->client->request('GET', $this->baseUrl . $url);
            $content = $res->getBody()->getContents();

            return json_decode($content, true);
        } catch (ClientException $e) {
            \Log::error(__METHOD__ . ' ClientException ' . $e->getMessage());

            return [];
        } catch (GuzzleException $e) {
            \Log::error(__METHOD__ . ' GuzzleException ' . $e->getMessage());

            return [];
        }
    }
}