<?php

namespace App\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;

class Riot
{
    private $client;
    private $baseUrl;
    private $puuid;
    private $apiKey;
    private $fetchCount;
    private $summonerId;

    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->baseUrl = config('riot.base_url');
        $this->puuid = config('riot.puuid');
        $this->apiKey = config('riot.api_key');
        $this->fetchCount = config('riot.fetch_count');
        $this->summonerId = config('riot.summoner_id');
    }

    public function getMatches(): array
    {
        return $this->get('/matches/by-puuid/' . $this->puuid . '/ids?count=' . $this->fetchCount);
    }

    public function getMatch(string $match): array
    {
        return $this->get('/matches/' . $match);
    }

    public function getSummonerRank(): array
    {
        return $this->get(
            '/entries/by-summoner/' . $this->summonerId,
            'https://na1.api.riotgames.com/tft/league/v1'
        );
    }

    private function get(string $url, $baseUrl = null): array
    {
        if (!$baseUrl) {
            $baseUrl = $this->baseUrl;
        }

        try {
            $res = $this->client->request('GET', $baseUrl . $url, [
                'headers' => [
                    'X-Riot-Token' => $this->apiKey
                ]
            ]);
            $content = $res->getBody()->getContents();

            return json_decode($content, true);
        } catch (ClientException $e) {
            \Log::error(__METHOD__ . ' ClientException ' . $e->getMessage());

            return [];
        } catch (GuzzleException $e) {
            \Log::error(__METHOD__ . ' GuzzleException ' . $e->getMessage());

            return [];
        }
    }
}
