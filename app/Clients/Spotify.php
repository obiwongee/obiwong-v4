<?php

namespace App\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;

class Spotify
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function authorize(string $code): void
    {
        try {
            $res = $this->client->post('https://accounts.spotify.com/api/token', [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'code' => $code,
                    'redirect_uri' => config('spotify.redirect_uri')
                ],
                'headers' => [
                    'Authorization' => 'Basic ' . config('spotify.encrypted_secret')
                ],
            ]);

            $content = $res->getBody()->getContents();
            $data = json_decode($content, true);


            Cache::put('spotify:access-token', $data['access_token'], now()->minutes(90));
            Cache::put('spotify:refresh-token', $data['refresh_token']);
        } catch (ClientException $e) {
            \Log::info(__METHOD__ . ': ' . $e->getMessage());
        }
    }

    public function fetchCurrentSong(): array
    {
        if ($currentSong = Cache::get('spotify:current-song')) {
            return json_decode($currentSong, true);
        }

        $accessToken = $this->getAccessToken();

        try {
            $res = $this->client->get('https://api.spotify.com/v1/me/player', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $accessToken
                ],
            ]);
            $content = $res->getBody()->getContents();

            if (empty($content)) {
                return [];
            }

            $songData = json_decode($content, true);
            $songDuration = $songData['item']['duration_ms'];
            $songProgress = $songData['progress_ms'];
            $cacheTime = ceil(($songDuration - $songProgress) / 1000);

            Cache::put('spotify:current-song', $content, now()->addSeconds($cacheTime));

            return json_decode($content, true);
        } catch (ClientException $e) {
            \Log::error(__METHOD__ . ': ' . $e->getMessage());

            return [];
        }
    }

    public function fetchTracks(): array
    {
        $accessToken = $this->getAccessToken();

        try {
            $res = $this->client->get('https://api.spotify.com/v1/me/top/tracks?time_range=short_term&limit=10', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $accessToken
                ],
            ]);
            $content = $res->getBody()->getContents();

            Cache::put('spotify:tracks', $content, now()->addWeek());

            return json_decode($content, true);
        } catch (ClientException $e) {
            \Log::error(__METHOD__ . ': ' . $e->getMessage());

            return [];
        }
    }

    public function refreshToken()
    {
        $refreshToken = Cache::get('spotify:refresh-token');

        try {
            $res = $this->client->post('https://accounts.spotify.com/api/token', [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $refreshToken,
                ],
                'headers' => [
                    'Authorization' => 'Basic ' . config('spotify.encrypted_secret')
                ],
            ]);

            $content = $res->getBody()->getContents();
            $data = json_decode($content, true);

            Cache::put('spotify:access-token', $data['access_token'], now()->minutes(90));
        } catch (ClientException $e) {
            \Log::info(__METHOD__ . ': ' . $e->getMessage());
        }
    }

    private function getAccessToken()
    {
        $accessToken = Cache::get('spotify:access-token');

        if (!$accessToken) {
            $this->refreshToken();

            $accessToken = Cache::get('spotify:access-token');
        }

        return $accessToken;
    }
}