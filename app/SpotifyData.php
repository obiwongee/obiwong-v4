<?php

namespace App;

use Illuminate\Support\Facades\Cache;

class SpotifyData
{
    public static function getTracks(): array
    {
        $data = Cache::get('spotify:tracks');

        if ($data) {
            $formatted = [];
            $tracks = json_decode($data, true);

            foreach ($tracks['items'] as $track) {
                $artists = [];
                foreach ($track['artists'] as $artist) {
                    $artists[] = $artist['name'];
                }

                $formatted[] = [
                    'artist' => implode(', ', $artists),
                    'title' => $track['name'],
                    'album_art' => $track['album']['images'][1]['url'],
                    'url' => $track['external_urls']['spotify']
                ];
            }

            return $formatted;
        }

        return [];
    }

    public static function getCurrentSong(): array
    {
        $data = Cache::get('spotify:current-song');


        if ($data) {
            $song = json_decode($data, true)['item'];
            $artists = [];

            foreach ($song['album']['artists'] as $artist) {
                $artists[] = $artist['name'];
            }

            $title = $song['name'];

            if (mb_strlen($title) > 28) {
                $title = mb_substr($title, 0, 25) . '...';
            }

            return [
                'artist' => $artists[0],
                'title' => $title,
                'url' => $song['external_urls']['spotify'],
            ];
        }

        return [];
    }
}