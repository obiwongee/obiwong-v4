<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class IpLocationData
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getIpLocation(string $encryptedIp): array
    {
        $location = $this->getLocation($encryptedIp);

        if (!empty($location)) {
            $locationDetails = $this->getLocationDetails($location);

            $weather = $this->getWeather($locationDetails['city'], $locationDetails['state_prov']);

            if (!empty($weather)) {
                $locationDetails['weather'] = $this->parseWeatherDetails($weather, $locationDetails['city']);
            }

            return $locationDetails;
        }

        return [];
    }

    private function getLocation(string $encryptedIp): array
    {
        $content = Cache::get("ip::$encryptedIp");

        if ($content) {
            return json_decode($content, true);
        }

        $baseUrl = config('iplocation.ipgeolocation.url');
        $apiKey = config('iplocation.ipgeolocation.api_key');

        try {
            $ip = decrypt($encryptedIp);

            $url = "$baseUrl/ipgeo?apiKey=$apiKey&ip=$ip";

            $res = $this->client->get($url);
            $content = $res->getBody()->getContents();

            Cache::put("ip::$encryptedIp", $content);

            return json_decode($content, true);
        } catch (\Exception $e) {
            \Log::info(__METHOD__ . ' : ' . $e->getMessage());
        }

        return [];
    }

    private function getLocationDetails(array $locationDetails): array
    {
        $json = file_get_contents(storage_path('/flag-emojis.json'));
        $flagEmojis = collect(json_decode($json, true))->keyBy('code');

        return [
            'country' => $locationDetails['country_name'],
            'state_prov' => $locationDetails['state_prov'],
            'city' => !empty($locationDetails['city']) ? $locationDetails['city'] : $locationDetails['country_capital'],
            'isp' => !empty($locationDetails['organization']) ? $locationDetails['organization'] : $locationDetails['isp'],
            'emoji' => $flagEmojis->get($locationDetails['country_code2'])['emoji'],
        ];
    }

    private function getWeather(string $city, string $state): array
    {
        $content = Cache::get("weather::$city$state");

        if ($content) {
            return json_decode($content, true);
        }

        $baseUrl = config('iplocation.openweather.url');
        $apiKey = config('iplocation.openweather.api_key');

        try {
            $url = "$baseUrl/weather?units=metric&q={$city},{$state}&appid={$apiKey}";

            $res = $this->client->get($url);
            $content = $res->getBody()->getContents();

            Cache::put("weather::$city$state", $content, now()->endOfDay());

            return json_decode($content, true);
        } catch (Exception $e) {
            \Log::info(__METHOD__ . ' : ' . $e->getMessage());
        }

        return [];
    }

    private function parseWeatherDetails(array $weather, string $city): string
    {
        $temperature = $weather['main']['feels_like'];

        if ($temperature < -20) {
            $feelsLike = 'freezing';
            $emoji = '🥶';
        } else if ($temperature < -10) {
            $feelsLike = 'very cold';
            $emoji = '🥶';
        } else if ($temperature < 0) {
            $feelsLike = 'cold';
            $emoji = '🥶';
        } else if ($temperature < 10) {
            $feelsLike = 'cool';
            $emoji = '😁';
        } else if ($temperature < 20) {
            $feelsLike = 'warm';
            $emoji = '☺️';
        } else {
            $feelsLike = 'hot';
            $emoji = '😄';
        }

        $main = [
            'Thunderstorm' => '$feelsLike stormy day ⛈',
            'Drizzle' => "$feelsLike day with light rainy 🌧",
            'Rain' => "$feelsLike rainy day 🌧",
            'Snow' => "$feelsLike snowy day 🌨",
            'Mist' => "$feelsLike foggy day 🌫",
            'Fog' => "$feelsLike foggy day 🌫",
            'Clear' => $temperature < -10 ? "sunny $feelsLike day ☀️" : "beautiful $feelsLike day ☀️",
            'Clouds' => "$feelsLike cloudy day ☁️",
        ];

        $detailed = [
            'few clouds' => "$feelsLike partly cloudy day 🌤",
            'scattered clouds' => "$feelsLike partly cloudy day 🌥",
            'Heavy snow' => "$feelsLike day with heavy snow ❄️"
        ];

        $weatherMain = $weather['weather'][0]['main'];
        $weatherDetail = $weather['weather'][0]['description'];

        if (array_key_exists($weatherDetail, $detailed)) {
            $condition = $detailed[$weatherDetail];
        } else if (array_key_exists($weatherMain, $main)) {
            $condition = $main[$weatherMain];
        } else {
            \Log::info($weather);

            $condition = $feelsLike . ' day';
        }

        if ($main[$weatherMain] == 'Clear' && ($feelsLike == 'hot' || $feelsLike == 'warm')) {
            $emoji = '😎';
        }

        return "Looks like it's a $condition in $city $emoji";
    }
}