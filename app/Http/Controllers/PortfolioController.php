<?php

namespace App\Http\Controllers;

use App\Decks\Deck;
use App\Favourites\Favourite;
use App\MarvelSnap;
use App\Matches\Match;
use App\SpotifyData;
use App\TFTData;
use App\TftStats\TftStat;
use App\TftStats\TftStatsRecent;
use App\Twitter\Tweets;
use Illuminate\Support\Facades\Cache;
use Jenssegers\Agent\Agent;

class PortfolioController
{
    private $projects = [
        [
            'name' => 'Personal home page',
            'link' => 'https://obiwong.com',
            'description' => 'This web page 😋',
        ],
        [
            'name' => 'Wordle Journle',
            'link' => 'https://wordlejournle.com/explore',
            'description' => "Start journlin' your Wordlin'",
        ],
        [
            'name' => 'Manabase',
            'link' => 'https://manabase.gg',
            'description' => 'Mana base calculator for Magic: The Gathering',
        ],
        [
            'name' => 'Fortress Maximus for Transformers TCG',
            'link' => 'https://fortressmaximus.io',
            'description' => "Card database and search engine for Transformers TCG",
        ],
        [
            'name' => 'Shared Secret',
            'link' => 'https://sharedsecret.app',
            'description' => "Stop sharing your secrets on Slack!",
            'source' => 'https://bitbucket.org/obiwongee/sharedsecret/src/main/'
        ],
        [
            'name' => 'Forecastly',
            'link' => 'https://weather.obiwong.com',
            'description' => "Smarter weather, better days",
        ],
        [
            'name' => 'React Demo',
            'link' => 'https://mtgreact.obiwong.com',
            'description' => "React demo for filtering and sorting",
            'source' => 'https://bitbucket.org/obiwongee/mtg-react/src/main/',
        ],
        [
            'name' => "My links",
            'link' => 'https://obiwong.com/links',
            'description' => "We have linktree at home",
        ],
    ];

    public function index(Agent $agent)
    {
        $start = request()->server('REQUEST_TIME_FLOAT');
        $tftStats = TftStat::latest()->first();
        $tftStatsRecent = TftStatsRecent::latest()->first();
        $decks = Deck::orderBy('id', 'desc')->get();
        $tweets = [];
        $lastMatches = Match::where('set', 'set4')
            ->orderBy('game_datetime', 'desc')->limit(3)->get();
        $favourites = new Favourite();
        $tftData = new TFTData();
        $topTracks = SpotifyData::getTracks();
        $snapStats = MarvelSnap::latest('created_at')->first();

        $styleCss = Cache::get(public_path('/portfolio/assets/css/style.css'));
        $scriptJs = Cache::get(public_path('/portfolio/assets/js/script.js'));
        $mainJs = Cache::get(public_path('/portfolio/assets/js/main.js'));

        if (request()->query('theme') == 'jace') {
            $theme = [
                'tag' => 'The Mind Sculptor',
                'css' => 'jace.css',
                'profile' => 'jace-profile.jpg',
            ];
        } else {
            $theme = [
                'tag' => 'Knight of the Old Republic',
                'css' => 'obi.css',
                'profile' => 'obi-profile.jpg'
            ];
        }

        $theme['css'] .= '?' . Cache::get(public_path('/portfolio/assets/css/' . $theme['css']));

        if ($agent->isRobot()) {
            $location = 'noip';
        } else if (!empty(config('iplocation.test_ip'))) {
            $location = encrypt(config('iplocation.test_ip'));
        } else {
            $location = encrypt(request()->ip());
        }

        return view('portfolio.index', [
            'tftStats' => $tftStats,
            'tftStatsRecent' => $tftStatsRecent,
            'lastMatches' => $lastMatches,
            'decks' => $decks,
            'tweets' => $tweets,
            'favourites' => $favourites,
            'tftData' => $tftData,
            'styleCss' => $styleCss,
            'scriptJs' => $scriptJs,
            'mainJs' => $mainJs,
            'topTracks' => $topTracks,
            'loadTime' => round((microtime(true) - $start) * 1000, 2),
            'agent' => $agent,
            'location' => $location,
            'theme' => $theme,
            'snapStats' => $snapStats,
            'projects' => $this->projects
        ]);
    }
}