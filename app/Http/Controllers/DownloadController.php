<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;

class DownloadController extends Controller
{
    public function index(string $filename)
    {
        $path = storage_path('/app/public/' . $filename);

        if (!file_exists($path)) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return response()->download($path);
    }
}