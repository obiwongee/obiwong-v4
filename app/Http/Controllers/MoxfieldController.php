<?php

namespace App\Http\Controllers;

use App\Decks\Deck;

class MoxfieldController extends Controller
{
    public function index()
    {
        $decks = Deck::all();

        return view('moxfield', [
            'decks' => $decks
        ]);
    }
}