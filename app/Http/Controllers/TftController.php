<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

class TftController extends Controller
{
    public function index(): View
    {
        return view('tft');
    }
}
