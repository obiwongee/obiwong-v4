<?php

namespace App\Http\Controllers;

class LinksController extends Controller
{
    public function index()
    {
        $links = [
            [
                'site' => 'obiwong.com',
                'url' => 'https://obiwong.com',
                'description' => 'My homepage portfolio'
            ],
            [
                'site' => 'Fortress Maximus',
                'url' => 'https://fortressmaximus.io',
                'description' => 'Deck builder and search engine for the Transformer TCG'
            ],
            [
                'site' => 'Manabase',
                'url' => 'https://manabase.gg',
                'description' => 'Mana base calculator for Magic: The Gathering'
            ],
            [
                'site' => 'Shared Secret',
                'url' => 'https://sharedsecret.app',
                'description' => 'Stop sharing your secrets on Slack!'
            ],
            [
                'site' => 'Wordle Journle',
                'url' => 'https://wordlejournle.com',
                'description' => "Track all your Wordle victories!"
            ],
            [
                'site' => 'My Wordle Journle',
                'url' => 'https://wordlejournle.com/journle/obiwong',
                'description' => "My Wordle stats tracked on Wordle Journle"
            ],
            [
                'site' => 'Forecastly',
                'url' => 'https://weather.obiwong.com',
                'description' => "Smarter Weather, Better Days"
            ],
            [
                'site' => 'bitbucket.com',
                'url' => 'https://bitbucket.org/obiwongee/',
                'description' => 'My bitbucket'
            ],
            [
                'site' => 'I can make linktree',
                'url' => 'https://obiwong.com/links',
                'description' => "We have link tree at home"
            ],
        ];

        return view('links', [
            'links' => $links,
        ]);
    }
}