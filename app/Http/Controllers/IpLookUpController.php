<?php

namespace App\Http\Controllers;

use App\IpLocationData;
use Illuminate\Http\Response;

class IpLookUpController
{
    public function index(IpLocationData $ipLocationData, string $encryptedIp)
    {
        if (empty(config('iplocation.test_ip')) &&
            ($encryptedIp === 'noip' || config('app.env') == 'local')) {
            return [];
        }

        $location = $ipLocationData->getIpLocation($encryptedIp);

        if (!empty($location)) {
            return $location;
        }

        abort(Response::HTTP_SERVICE_UNAVAILABLE);
    }
}