<?php

namespace App\Http\Controllers;

use App\Clients\Spotify;
use App\SpotifyData;
use Illuminate\Http\Request;

class SpotifyController
{
    public function index()
    {
        $baseUrl = 'https://accounts.spotify.com/authorize';

        $params = [
            'response_type' => 'code',
            'client_id' => config('spotify.client_id'),
            'scope' => config('spotify.scopes'),
            'redirect_uri' => config('spotify.redirect_uri')
        ];

        return redirect("$baseUrl?" . http_build_query($params));
    }

    public function callback(Request $request, Spotify $spotify)
    {
        $code = $request->get('code');
        $spotify->authorize($code);
    }

    public function currentSong(Spotify $spotify)
    {
        $spotify->fetchCurrentSong();

        return SpotifyData::getCurrentSong();
    }
}