<?php

namespace App\Matches;

use App\TFTData;
use Illuminate\Database\Eloquent\Model;
use NumberFormatter;

class Match extends Model
{
    protected $fillable = [
        'match_id',
        'game_datetime',
        'game_length',
        'traits',
        'units',
        'players_eliminated',
        'total_damage_to_players',
        'placement',
        'set',
    ];

    const STANDARD = 'standard';

    public function setTraitsAttribute($value)
    {
        $this->attributes['traits'] = json_encode($value);
    }

    public function getTraitsAttribute(): array
    {
        return json_decode($this->attributes['traits'], true);
    }

    public function setUnitsAttribute($value)
    {
        $this->attributes['units'] = json_encode($value);
    }

    public function getUnitsAttribute()
    {
        return json_decode($this->attributes['units'], true);
    }

    public function getOrdinalPlacement()
    {
        $numberFormatter = new NumberFormatter('en_US', NumberFormatter::ORDINAL);

        return $numberFormatter->format($this->placement);
    }

    public function getSortedUnits(string $key)
    {
        return collect($this->units)
            ->filter(function($unit) {
                return $unit['character_id'] != 'TFT_TrainingDummy';
            })
            ->map(function($unit) {
                $unit['character_id'] = str_replace('TFT4b', 'TFT4',  $unit['character_id']);
                $unit['asset'] = asset("tft/{$this->set}/champions") . '/' . $unit['character_id'] . '.png';

                return $unit;
            })
            ->sortBy($key);
    }

    public function getSortedTraits(string $key)
    {
        $tftData = new TFTData($this->set);

        return collect($this->getTraits())
            ->map(function($trait) use ($tftData) {
                $trait['asset'] = asset("tft/{$this->set}/traits") . '/' . str_replace(' ', '', strtolower($tftData->getTraitName($trait['name']))) . '.svg';

                return $trait;
            })
            ->sortBy($key);
    }

    public function getChosen()
    {
        foreach ($this->units as $unit) {
            if (array_key_exists('chosen', $unit)) {
                return str_replace('Set4_', '', $unit['chosen']);
            }
        }

        return null;
    }

    public function getTraits()
    {
        $traits = [];
        foreach ($this->traits as $trait) {
            $trait['name'] = str_replace('Set4_', '', $trait['name']);
            $traits[] = $trait;
        }

        return $traits;
    }
}
