<?php

namespace App\Decks;

use Illuminate\Database\Eloquent\Model;

class Deck extends Model
{
    protected $fillable = [
        'moxfield_id'
    ];
}